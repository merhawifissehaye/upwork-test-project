const EMAIL_REGEX = new RegExp(
  /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i
);

abstract class User {
  private _userId: number;
  private _firstName: string;
  private _lastName: string;
  private _email: string | null;
  private _profilePhoto: string | null;

  constructor(
    userId: number,
    firstName: string,
    lastName: string,
    email: string | null = null,
    profilePhoto: string | null = null
  ) {
    this._userId = userId;
    this._firstName = firstName;
    this._lastName = lastName;
    this._email = email;
    this._profilePhoto = profilePhoto;
  }

  get fullName(): string {
    return this._firstName + ' ' + this._lastName;
  }

  get profilePicture(): string {
    // the url at the right end is assumed to be the default system avatar
    return this._profilePhoto ?? 'https://i.pravatar.cc/300';
  }

  get email(): string | null {
    return this._email;
  }

  get userId(): number {
    return this._userId;
  }

  saveUser(): boolean {
    // validate email
    if (this._email && !EMAIL_REGEX.test(this._email)) {
      throw new TypeError('Invalid email');
    }

    // valiate profile picture
    if (this._profilePhoto && !/\.jpg$/.test(this._profilePhoto)) {
      throw new TypeError('Invalid profile photo url');
    }

    return true;
  }
}

export default User;
