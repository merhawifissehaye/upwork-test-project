import Student from './student';
import Teacher from './teacher';
import User from './user';

const months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

export enum MessageType {
  System,
  Manual,
}

class Message {
  private _sender: User;
  private _receiver: User;
  private _text: string;
  private _createdAt: number; // timestamp
  private _type: MessageType; // system or manual

  constructor(
    sender: User,
    receiver: User,
    text: string,
    createdAt: number = Date.now(),
    type: MessageType = MessageType.Manual
  ) {
    this._sender = sender;
    this._receiver = receiver;
    this._text = text;
    this._createdAt = createdAt;
    this._type = type;
  }

  get senderFullName(): string {
    return this._sender.fullName;
  }

  get receiverFullName(): string {
    return this._receiver.fullName;
  }

  get messageType(): string {
    return this._type === MessageType.Manual
      ? 'Manual Message'
      : 'System Message';
  }

  set messageType(type: string) {
    this._type = /system/i.test(type) ? MessageType.System : MessageType.Manual;
  }

  private _prependZero(value: number): string {
    return value < 10 ? `0${value}` : `${value}`;
  }

  get createdAt(): string {
    const dateTime = new Date(this._createdAt);
    const year = dateTime.getFullYear();
    const month = months[dateTime.getMonth()];
    const date = dateTime.getDate();

    const hour = this._prependZero(dateTime.getHours());
    const minutes = this._prependZero(dateTime.getMinutes());
    const seconds = this._prependZero(dateTime.getSeconds());

    return `${month} ${date}, ${year} ${hour}:${minutes}:${seconds}`;
  }

  private _validateMessage(): void {
    if (
      this._type === MessageType.System &&
      (!(this._sender instanceof Teacher) ||
        !(this._receiver instanceof Student))
    ) {
      throw new Error(
        'System messages can only be sent from teacher to student'
      );
    }
  }

  save(): boolean {
    this._validateMessage();
    return true;
  }

  send(): boolean {
    this._validateMessage();
    return true;
  }
}

export default Message;
