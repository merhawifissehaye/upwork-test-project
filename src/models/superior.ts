import User from './user';

abstract class Superior extends User {
  private _salutation?: string | null;

  constructor(
    userId: number,
    firstName: string,
    lastName: string,
    email: string | null = null,
    profilePhoto: string | null = null,
    salutation: string | null = null
  ) {
    super(userId, firstName, lastName, email, profilePhoto);
    this._salutation = salutation;
  }

  get fullName(): string {
    return (this._salutation ? this._salutation + ' ' : '') + super.fullName;
  }
}

export default Superior;
