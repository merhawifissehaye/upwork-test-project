import Message, {MessageType} from '../src/models/message';
import Student from '../src/models/student';
import Teacher from '../src/models/teacher';

const teacher = new Teacher(
  1,
  'Merhawi',
  'Fissehaye',
  'merhawifissehaye@gmail.com'
);

const student = new Student(2, 'Great', 'Student', 'student@institute.com');

describe('Message', () => {
  test('it can create a new message object', () => {
    const message = new Message(teacher, student, 'Hi my dear student');
    expect(message).toBeInstanceOf(Message);
  });

  test('it throws exception if we try to save system message from non-teacher', () => {
    const message = new Message(
      student,
      teacher,
      'Hi my dear teacher',
      Date.now(),
      MessageType.System
    );
    expect(() => {
      message.save();
    }).toThrow();
  });

  test('it throws exception if we try to send system message from non-teacher', () => {
    const message = new Message(
      student,
      teacher,
      'Hi my dear teacher',
      Date.now(),
      MessageType.System
    );
    expect(() => {
      message.send();
    }).toThrow();
  });

  test('it can get full name of sender and receiver', () => {
    const message = new Message(teacher, student, 'Hi my dear student');
    expect(message.senderFullName).toEqual('Merhawi Fissehaye');
    expect(message.receiverFullName).toEqual('Great Student');
  });

  test('it can get message type', () => {
    const message = new Message(teacher, student, 'Hi my dear student');
    expect(message.messageType).toEqual('Manual Message');

    message.messageType = 'System';
    expect(message.messageType).toEqual('System Message');
  });

  test('it can get formatted creation time', () => {
    const d = new Date('Jan 11, 2022 03:42:12');
    const message = new Message(
      teacher,
      student,
      'Hi my dear student',
      d.getTime()
    );
    expect(message.createdAt).toEqual('Jan 11, 2022 03:42:12');
  });
});
