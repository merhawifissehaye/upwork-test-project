import Parent from '../src/models/parent';
import Student from '../src/models/student';
import Teacher from '../src/models/teacher';
import User from '../src/models/user';

const teacher = new Teacher(
  1,
  'Teacher',
  'Guides',
  'teacher@school.com',
  null,
  'Dr.'
);
const student = new Student(2, 'Student', 'Learns', 'student@school.com');
const parent = new Parent(
  3,
  'Parent',
  'Cares',
  'parent@school.com',
  null,
  'Mr.'
);

describe('User', () => {
  test('it can create a new teacher', () => {
    expect(teacher).toBeInstanceOf(User);
  });

  test('it can create a new student', () => {
    expect(student).toBeInstanceOf(User);
  });

  test('it can create a new parent', () => {
    expect(parent).toBeInstanceOf(User);
  });

  test('it can get student full name', () => {
    expect(student.fullName).toEqual('Student Learns');
  });

  test('it can get teacher full name', () => {
    expect(teacher.fullName).toEqual('Dr. Teacher Guides');
  });

  test('it can get parent full name', () => {
    expect(parent.fullName).toEqual('Mr. Parent Cares');
  });

  test('it can get profile picture', () => {
    const user = new Parent(1, 'Random', 'User', 'Mr.', 'profile url');
    expect(user.profilePicture).toEqual('profile url');
  });

  test('it can get default profile picture if not provided', () => {
    const user = new Parent(1, 'Random', 'User');
    expect(user.profilePicture).toEqual('https://i.pravatar.cc/300');
  });

  test('it can get email', () => {
    expect(teacher.email).toEqual('teacher@school.com');
  });

  test('it can get user id', () => {
    expect(student.userId).toEqual(2);
  });

  test('it throws error when saving invalid email', () => {
    const user = new Student(10, 'Random', 'Student', 'invalid-email');
    expect(() => {
      user.saveUser();
    }).toThrow('Invalid email');
  });

  test('it throws error when saving invalid profile picture', () => {
    const user = new Student(
      10,
      'Random',
      'Student',
      'random@user.com',
      'invalid-url'
    );
    expect(() => {
      user.saveUser();
    }).toThrow('Invalid profile photo url');
  });

  test('it saves', () => {
    const user = new Parent(
      20,
      'Random',
      'Student',
      'random@user.com',
      'photo-url.jpg'
    );

    expect(user.saveUser()).toEqual(true);
  });
});
